
This project has moved to https://github.com/mverleg/block_comments
==================================================================================

I am collecting all my projects in one place, which is Github, so development continues there!

Block comments (Firefox addon)
---------------------------------------

User content is an indispensable part of the internet. Think of all the awesome Youtube videos. But on some occasions, the content is better without frustrated people with too much time spewing their wordy ignorance all over your screen. Think of Youtube 96.4% of all Youtube comments.

This comic describes it well: http://www.smbc-comics.com/index.php?db=comics&id=759

Stil, it can be tempting to read it, and even reply. This addon is here to help you resist that urge, by simply hiding this wordy ignorance.


Included websites
---------------------------------------

These things are blocked:

* Youtube: comments
* 9gag: comments


(This list might not be entirely up-to-date)


Contributors
---------------------------------------

Any additional blocking rules are most welcome! The code is at https://bitbucket.org/mverleg/firefox_no_comments


License
---------------------------------------

Revised BSD License, see LICENSE.txt. You can use the addon as you choose, without warranty. The code is also available, see Contributors.

Other addons
---------------------------------------

* Hide distracting "read more" parts of some popular sites - https://addons.mozilla.org/firefox/addon/block_read_more/
* Hide comments on some popular sites where they are notoriously unconstructive [this one] - https://addons.mozilla.org/firefox/addon/block-comments/
* Hide the registration overlay on Quora - https://addons.mozilla.org/firefox/addon/quora-unfade/


